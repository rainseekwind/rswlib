#ifndef DISJOINT_SET_HPP
#define DISJOINT_SET_HPP

namespace rsw
{
class disjoint_set
{
  public:
    disjoint_set();
    disjoint_set(int size);

    ~disjoint_set();

  private:
    int m_size;
    int *m_fa;

  public:
    int get_size();
    int find(int x);
    void merge(int x, int y);
    bool query(int x, int y);
    void reset();
    void resize(int size);
};
}  // namespace rsw

#endif  // DISJOINT_SET_HPP
