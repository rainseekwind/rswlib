#include "RSWLog.hpp"
#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <ctime>

#if defined(__linux__)

  #define SET_OUTPUT_COLOR_RED printf("\033[40;31m")
  #define SET_OUTPUT_COLOR_GREEN printf("\033[40;32m")
  #define SET_OUTPUT_COLOR_YELLOW printf("\033[40;33m")
  #define SET_OUTPUT_COLOR_RED_WITH_YELLOW printf("\033[43;31m")
  #define RESET_OUTPUT_COLOR printf("\033[0m")

#elif defined(_WIN32)

  #include <Windows.h>
  #define SET_OUTPUT_COLOR_RED \
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED)
  #define SET_OUTPUT_COLOR_GREEN         \
    SetConsoleTextAttribute(             \
        GetStdHandle(STD_OUTPUT_HANDLE), \
        FOREGROUND_INTENSITY | FOREGROUND_GREEN)
  #define SET_OUTPUT_COLOR_YELLOW        \
    SetConsoleTextAttribute(             \
        GetStdHandle(STD_OUTPUT_HANDLE), \
        FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_RED)
  #define SET_OUTPUT_COLOR_RED_WITH_YELLOW                                            \
    SetConsoleTextAttribute(                                                          \
        GetStdHandle(STD_OUTPUT_HANDLE),                                              \
        FOREGROUND_INTENSITY | FOREGROUND_RED | BACKGROUND_INTENSITY | BACKGROUND_RED \
            | BACKGROUND_GREEN)
  #define RESET_OUTPUT_COLOR SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x07)

#else

  #define SET_OUTPUT_COLOR_RED (0)
  #define SET_OUTPUT_COLOR_GREEN (0)
  #define SET_OUTPUT_COLOR_YELLOW (0)
  #define SET_OUTPUT_COLOR_RED_WITH_YELLOW (0)
  #define RESET_OUTPUT_COLOR (0)

#endif

namespace rsw
{
rsw_log::rsw_log() : m_printable(true), m_buffer_size(1024 * 1024)
{
    set_file_path("\0");
}

rsw_log::rsw_log(const char *file_path) : m_printable(true), m_buffer_size(1024 * 1024)
{
    set_file_path(file_path);
}

rsw_log::~rsw_log()
{
    write_buffer();
}

void rsw_log::set_file_path(const char *file_path)
{
    if(strlen(file_path) >= 2048)
        throw false;  // TODO: update the exception management
    strcpy(m_file_path, file_path);
}

void rsw_log::set_printable(bool status)
{
    m_printable = status;
}

void rsw_log::rawlog(RSW_LOG_LEVEL level, const char *log)
{
    if(strlen(log) >= RSW_LOG_MAX_LEN)
        throw false;  // TODO: update the exception management

    // Get current time
    time_t now = time(nullptr);
    tm *tm_t = localtime(&now);

    // Push log into buffer
    rsw_log_unit log_buf;
    log_buf.level = level;
    log_buf.date = (tm_t->tm_year + 1900) * 10000 + (tm_t->tm_mon + 1) * 100 + (tm_t->tm_mday);
    log_buf.time = (tm_t->tm_hour) * 10000 + (tm_t->tm_min) * 100 + (tm_t->tm_sec);
    strcpy(log_buf.log, log);
    m_buffer.push(log_buf);

    // Print the log to the screen
    if(m_printable) {
        printf(
            "[%04d-%02d-%02d][%02d:%02d:%02d][",
            tm_t->tm_year + 1900,
            tm_t->tm_mon + 1,
            tm_t->tm_mday,
            tm_t->tm_hour,
            tm_t->tm_min,
            tm_t->tm_sec);
        if(level == RSW_LOG_LEVEL_INFO)
            SET_OUTPUT_COLOR_GREEN, printf("INFO");
        else if(level == RSW_LOG_LEVEL_WARNING)
            SET_OUTPUT_COLOR_YELLOW, printf("WARNING");
        else if(level == RSW_LOG_LEVEL_ERROR)
            SET_OUTPUT_COLOR_RED, printf("ERROR");
        else
            SET_OUTPUT_COLOR_RED_WITH_YELLOW, printf("FATAL");
        RESET_OUTPUT_COLOR;

        putchar(']'), putchar(' ');
        for(const char *p = log; *p != '\0'; p++)
            if(*p == '\n')
                putchar('\n'), putchar('>'), putchar('>'), putchar(' ');
            else
                putchar(*p);
        putchar('\n');
    }
}

void rsw_log::log(RSW_LOG_LEVEL level, const char *log, ...)
{
    char buf[RSW_LOG_MAX_LEN];
    va_list args;
    int n;
    va_start(args, log);
    n = vsprintf(buf, log, args);
    va_end(args);
    rawlog(level, buf);
}

void rsw_log::info(const char *log, ...)
{
    char buf[RSW_LOG_MAX_LEN];
    va_list args;
    int n;
    va_start(args, log);
    n = vsprintf(buf, log, args);
    va_end(args);
    rawlog(RSW_LOG_LEVEL_INFO, buf);
}

void rsw_log::warning(const char *log, ...)
{
    char buf[RSW_LOG_MAX_LEN];
    va_list args;
    int n;
    va_start(args, log);
    n = vsprintf(buf, log, args);
    va_end(args);
    rawlog(RSW_LOG_LEVEL_WARNING, buf);
}

void rsw_log::error(const char *log, ...)
{
    char buf[RSW_LOG_MAX_LEN];
    va_list args;
    int n;
    va_start(args, log);
    n = vsprintf(buf, log, args);
    va_end(args);
    rawlog(RSW_LOG_LEVEL_ERROR, buf);
}

void rsw_log::fatal(const char *log, ...)
{
    char buf[RSW_LOG_MAX_LEN];
    va_list args;
    int n;
    va_start(args, log);
    n = vsprintf(buf, log, args);
    va_end(args);
    rawlog(RSW_LOG_LEVEL_FATAL, buf);
}

void rsw_log::write_buffer()
{
    if(m_file_path[0] == '\0' || m_buffer.empty())
        return;

    FILE *pfile;
    if((pfile = fopen(m_file_path, "a")) == nullptr)
        throw false;  // TODO: exception management

    char tmp[RSW_LOG_MAX_LEN + 1024];

    while(!m_buffer.empty()) {
        sprintf(tmp, "%08ld %06ld ", m_buffer.front().date, m_buffer.front().time);
        if(m_buffer.front().level == RSW_LOG_LEVEL_INFO)
            strcat(tmp, "INFO ");
        else if(m_buffer.front().level == RSW_LOG_LEVEL_WARNING)
            strcat(tmp, "WARNING ");
        else if(m_buffer.front().level == RSW_LOG_LEVEL_ERROR)
            strcat(tmp, "ERROR ");
        else
            strcat(tmp, "FATAL ");
        strcat(tmp, m_buffer.front().log);
        strcat(tmp, "\n");

        fwrite(tmp, strlen(tmp), 1, pfile);
        m_buffer.pop();
    }

    fclose(pfile);
}

void rsw_log::set_buffer_size(long size)
{
    m_buffer_size = size;
}
}  // namespace rsw
