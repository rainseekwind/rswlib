#ifndef RSW_LOG_HPP
  #include <queue>

namespace rsw
{
enum RSW_LOG_LEVEL {
    RSW_LOG_LEVEL_INFO,
    RSW_LOG_LEVEL_WARNING,
    RSW_LOG_LEVEL_ERROR,
    RSW_LOG_LEVEL_FATAL
};

const int RSW_LOG_MAX_LEN = 2048;

struct rsw_log_unit {
    RSW_LOG_LEVEL level;
    long date;  // Format: yyyy/mm/dd
    long time;  // Format: hh/mm/ss
    char log[RSW_LOG_MAX_LEN];
};

class rsw_log
{
  public:
    rsw_log();
    rsw_log(const char *file_path);
    ~rsw_log();

  private:
    char m_file_path[2048];             // The file path to save the log
    bool m_printable;                   // Whether print the log to stdout or not
    std::queue<rsw_log_unit> m_buffer;  // The buffer of the log
    long m_buffer_size;

  public:
    void set_file_path(const char *file_path);  // With a length limit as 2048
    void set_printable(bool status);
    void rawlog(RSW_LOG_LEVEL level,
                const char *log);  // With a length limit as RSW_LOG_MAX_LEN
    void log(RSW_LOG_LEVEL level, const char *log, ...);
    void info(const char *log, ...);
    void warning(const char *log, ...);
    void error(const char *log, ...);
    void fatal(const char *log, ...);
    void write_buffer();  // Write the buffer into file
    void set_buffer_size(long size);
};
}  // namespace rsw

  #define RSW_LOG_HPP
#endif
