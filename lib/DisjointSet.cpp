#include "DisjointSet.hpp"
#include <cstring>

namespace rsw
{
disjoint_set::disjoint_set() : m_size(32)
{
    m_fa = new int[m_size];
    reset();
}

disjoint_set::disjoint_set(int size) : m_size(size)
{
    m_fa = new int[m_size];
    reset();
}

disjoint_set::~disjoint_set()
{
    delete[] m_fa;
    m_fa = nullptr;
}

int disjoint_set::get_size()
{
    return m_size;
}

int disjoint_set::find(int x)
{
    return m_fa[x] == x ? x : m_fa[x] = find(m_fa[x]);
}

void disjoint_set::merge(int x, int y)
{
    m_fa[find(x)] = find(y);
}

bool disjoint_set::query(int x, int y)
{
    return find(x) == find(y);
}

void disjoint_set::reset()
{
    for(int i = 0; i < m_size; i++) m_fa[i] = i;
}

void disjoint_set::resize(int size)
{
    delete[] m_fa;
    m_size = size;
    m_fa = new int[m_size];
    reset();
}
}  // namespace rsw