#include "RSWLog.hpp"
#include <cstdio>
#include <cstdlib>

using namespace rsw;

int main()
{
    /*
     * Debug for RSWLog.hpp/cpp*/
    rsw_log l;

    l.log(RSW_LOG_LEVEL_INFO, "Test-Info");
    l.log(RSW_LOG_LEVEL_WARNING, "Test-Warning");
    l.log(RSW_LOG_LEVEL_ERROR, "Test-Error");
    l.log(RSW_LOG_LEVEL_FATAL, "Test-Fatal");
    l.info("Test-Direct-Info");
    l.warning("Test-Direct-Warning");
    l.error("Test-Direct-Error");
    l.fatal("Test-Direct-Fatal");
    l.info("Test format output %04d %.6f", 101, 3.14);
    l.info("Test\nfor multi-line\nlog");

    l.set_file_path("./test.log");

    /*
    * Debug for DisjointSet.hpp/cpp
    * Luogu P3367
    int n, m;
    scanf("%d%d", &n, &m);
    disjoint_set ds;
    ds.resize(n);
    while(m--)
    {
        int op, x, y;
        scanf("%d%d%d", &op, &x, &y);
        x--, y--;
        if(op == 1)
            ds.merge(x, y);
        else
            printf("%c\n", ds.query(x, y) ? 'Y' : 'N');
    } */
#ifdef _WIN32
    system("pause");
#endif
    return 0;
}
